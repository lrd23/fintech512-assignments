function getStockData(symbol) {
  const api_key = os.environ.get('API_KEY')
  const INTERVAL = "5min"; 
  const apiUrl = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${symbol}&interval=${INTERVAL}&apikey=${API_KEY}";

  fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
      const timeSeriesData = data[`Time Series (${INTERVAL})`];
      const latestData = timeSeriesData[Object.keys(timeSeriesData)[0]];
      const latestPrice = parseFloat(latestData["4. close"]).toFixed(2);
      const latestTime = Object.keys(timeSeriesData)[0];

      // display the stock price and time
      const priceElement = document.getElementById("stock-price");
      priceElement.textContent = `$${latestPrice}`;

      const timeElement = document.getElementById("stock-time");
      timeElement.textContent = `Last updated: ${latestTime}`;
    })
    .catch(error => {
      console.error(`Error getting stock data for ${symbol}: ${error}`);
    });
}