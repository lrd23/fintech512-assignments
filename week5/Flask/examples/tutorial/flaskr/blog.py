import os
import json
from flask import Flask
from flask import render_template
from flask import request
from pip._vendor import requests
from flask import Blueprint
import plotly.graph_objs as go

blog_bp = Blueprint('blog', __name__)

api_key = os.environ.get('API_KEY')

@blog_bp.route('/')
def base():
    return render_template('index.html')

@blog_bp.route('/stock', methods=['POST'])
def get_stock_info():
    symbol = request.form['symbol']
    url = f"https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={symbol}&interval=5min&apikey={api_key}"
    response = requests.get(url)
    data = response.json()
    timeSeriesData = data.get("Time Series (5min)", {})
    
    #Get company overview data
    overview_url = f"https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={api_key}"
    overview_response = requests.get(overview_url)
    overview_data = overview_response.json()
    eps = overview_data.get("EPS")
    pe_ratio = overview_data.get("PERatio")
    company_name = overview_data.get("Name")
    sector = overview_data.get("Sector")
    industry = overview_data.get("Industry")
    dividend_yield = overview_data.get("DividendYield")
    dividend_ps = overview_data.get("DividendPerShare")
    market_cap = overview_data.get("MarketCapitalization")
    high = overview_data.get("52WeekHigh")
    low = overview_data.get("52WeekLow")
    exchange = overview_data.get("Exchange")

    # Get the stock's historical data
    historical_url = f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={symbol}&apikey={api_key}"
    historical_response = requests.get(historical_url)
    historical_data = historical_response.json()
    historical_time_series_data = historical_data.get(
        "Time Series (Daily)", {})
    historical_dates = list(historical_time_series_data.keys())
    historical_closing_prices = [float(
        historical_time_series_data[date]["4. close"]) for date in historical_dates]
    historical_dates.reverse()
    historical_closing_prices.reverse()
    
    #Get latest news stories
    news_url = f"https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={symbol}&apikey={api_key}"
    news_response = requests.get(news_url)
    news_data = news_response.json()
    result = []
    feed = news_data["feed"]
    for record in feed:
            news_items = {"title": record["title"], "url": record["url"], "summary": record["summary"], "published": record["time_published"]}
            result.append(news_items)
            if len(result) >= 5:
                break
            titles = news_data.get("feed", {})
            
    

    # Create the chart using plotly
    chart_data = go.Scatter(x=historical_dates, y=historical_closing_prices)
    layout = go.Layout(title=f"Closing Prices for {company_name} (Past Year)", xaxis=dict(
        title="Date"), yaxis=dict(title="Closing Price"))
    chart = go.Figure(data=[chart_data], layout=layout)
    chart_html = chart.to_html(full_html=False)
    return render_template('stock_info.html', symbol=symbol, interval="5min", function="TIME_SERIES_INTRADAY", timeSeriesData=timeSeriesData, eps=eps, pe_ratio=pe_ratio, company_name=company_name, sector=sector, industry=industry, dividend_yield=dividend_yield, dividend_ps= dividend_ps, market_cap=market_cap, high=high, low=low, exchange=exchange, chart_html=chart_html, news_items=news_items, titles=titles)

@blog_bp.route('/stock_info', methods=['GET'])
def stock_info():
    symbol = request.args.get("symbol")
    interval = request.args.get("interval")
    function = request.args.get("function")
    timeSeriesData = json.loads(request.args.get("timeSeriesData"))
    eps = request.args.get("eps")
    pe_ratio = request.args.get("pe_ratio")
    company_name = request.args.get("company_name")
    sector = request.args.get("sector")
    industry = request.args.get("industry")
    dividend_yield = request.args.get("DividendYield")
    dividend_ps = request.args.get("DividendPerShare")
    market_cap = request.args.get("MarketCapitalization")
    high = request.args.get("52WeekHigh")
    low = request.args.get("52WeekLow")
    exchange = request.args.get("Exchange")
    chart_html = request.args.get("chart")
    return render_template('stock_info.html', symbol=symbol, interval=interval, function=function, timeSeriesData=timeSeriesData, eps=eps, pe_ratio=pe_ratio, company_name=company_name, sector=sector, industry=industry, dividend_yield=dividend_yield, dividend_ps=dividend_ps, market_cap=market_cap, high=high, low=low, exchange=exchange, chart_html=chart_html)


blog_app = Flask(__name__)
blog_app.register_blueprint(blog_bp)

if __name__ == '__main__':
    blog_app.run()
