import os
from flask import render_template
from flask import request
from flask import Blueprint
from pip._vendor import requests
from datetime import datetime
from flask import url_for
from flask import redirect
from flaskr.db import get_db
from flaskr.db import init_db

blog_bp = Blueprint('blog', __name__)
api_key = os.environ.get('API_KEY')


@blog_bp.route("/")
def index():
    # init_db()
    db = get_db()
    stock = db.execute(
        "SELECT DISTINCT symbol, date_added, tracking_price, num_shares, current_price, percent_change "
        "FROM stock "
        "ORDER BY date_added DESC"
    ).fetchall()

    return render_template("index.html", stock=stock)


@blog_bp.route('/add_stock', methods=['POST'])
def add_stock():
    if request.method == "POST":
        symbol = request.form['symbol']
        tracking_price = request.form['tracking_price']
        num_shares = request.form['num_shares']
        current_price = get_current_price(symbol)
        if current_price is None:
            return "Invalid stock symbol"
        percent_change = (current_price - float(tracking_price)) / float(tracking_price) * 100
        date_added = datetime.utcnow()

        db = get_db()
        db.execute(
            'INSERT INTO stock(symbol, date_added, tracking_price, num_shares, current_price, percent_change) VALUES (?, ?, ?, ?, ?, ?)',
            (symbol, date_added, tracking_price,
             num_shares, current_price, percent_change))

        db.commit()

        return redirect(url_for('blog.stock_tracker'))

@blog_bp.route('/stock_tracker')
def stock_tracker():
    db = get_db()
    stock = db.execute(
        'SELECT * FROM stock ORDER BY date_added DESC').fetchall()
    return render_template('tracker.html', stock=stock)


@blog_bp.route('/delete_stock/<int:id>', methods=['GET', 'POST'])
def delete_stock(id):
    db = get_db()
    db.execute("DELETE FROM stock WHERE id = ?", (id,))
    db.commit()
    return redirect(url_for('blog.stock_tracker'))


def get_current_price(symbol):
    url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={api_key}'
    try:
        response = requests.get(url).json()
        return float(response['Global Quote']['05. price'])
    except:
        return None


