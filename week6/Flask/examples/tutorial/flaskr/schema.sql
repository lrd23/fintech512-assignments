-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS stock;

CREATE TABLE stock (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  symbol TEXT NOT NULL,
  date_added DATETIME NOT NULL,
  tracking_price FLOAT NOT NULL,
  num_shares INTEGER NOT NULL,
  current_price FLOAT NOT NULL,
  percent_change FLOAT NOT NULL
);

